﻿#include "foo.h"
#include <iostream>
#include <string>
#include <vector>

int main(int argc, char** argv) {





    // 里 main 远点是为了腾出空间好查看 fdatetime 变量的调试信息
    FXXDateTime fdatetime(2024, 12, 22, 05, 53, 29);  // 添加到这里, 在下面一句打断点, 调试运行后将鼠标悬停在fdatetime变量上

    std::cout << "开始打印程序的参数！" << std::endl;
    for (int i = 0; i < argc; ++i)
    {
        std::cout << "参数" << i << " " << argv[i]  << std::endl;            
    }
    std::cout << "结束打印程序的参数！" << std::endl;

    std::cout << "验证Attach功能" << std::endl;
    std::cout << "第一步, 在std::cin >> waitAttach;下一句打断点" << std::endl; 
    std::cout << "第二步, 启动eg_prjs程序" << std::endl; 
    std::cout << "第三步, 在调试界面 选择 cppvsdbg attech" << std::endl; 
    std::cout << "第四步, 点击绿色执行按钮" << std::endl;
    std::cout << "第五步, 附加 eg_prjs 进程. 然后在 eg_prjs 控制台界面中输入字符回车后, 会自动停止打断点处"<< std::endl;  
    std::cout << "等待输入:" << std::endl;
    std::string waitAttach;
    std::cin >> waitAttach;
    std::cout << "输入值是:" << waitAttach  << std::endl;  


    std::cout << "add(1, 2) = " << add(1, 2) << std::endl;

    std::vector<std::string> stdvec = { "Hello", "vscode", "xmake", "C/C++", "!!!" };
    for (auto &&i : stdvec)
    {
        std::cout  << i << " ";
    }
    std::cout << std::endl;    
    
    std::cout << "Hello launch.json" << std::endl;        


    return 0;
}
