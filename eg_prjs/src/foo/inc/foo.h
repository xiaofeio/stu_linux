﻿#ifdef __cplusplus
extern "C" {
#endif

#if defined(_WIN32)
#   define __export         __declspec(dllexport)
#elif defined(__GNUC__) && ((__GNUC__ >= 4) || (__GNUC__ == 3 && __GNUC_MINOR__ >= 3))
#   define __export         __attribute__((visibility("default")))
#else
#   define __export
#endif

__export int add(int a, int b);

#ifdef __cplusplus
}
#endif

/**
 * brief 日期时间. 为了避免与常用的名称区分, 定义名称为 FXXDateTime
 * 成员变量就是 年/月/日 小时:分钟:秒
 */
class __export FXXDateTime
{
public:
    FXXDateTime(int year, int month, int day, int hour, int minute, int second);
    ~FXXDateTime();


/// 头文件除了提供API外, 更重要的是让使用者通过查看头文件就知道怎么使用(谁还看文档[文档该写还是的得写], 除非必要, 大家用微信有看过微信说明吗),
/// 那么使用者最关心的就是public部分, 根本就不看private部分, protected部分根据情况才看
/// 私有的放在前面, 在阅读的时候还要把代码拉到下面, 不觉的麻烦么. 
/// 我的理念是private部分放在类的最后面, 其次是protected部分, public放在类的最前面. 因为实际开发过程中我也从不看私有(除非必要).
/// 况且类似Qt库等做法, 把私有数据完全封装到一个data的结构体中, 类中只有一个data的指针, 继承类连data的指针都看不见.
/// 这里只是演示, 定义成员变量如下
/// @cond
private:
    int m_year;
    int m_month;
    int m_day;
    
    int m_hour;
    int m_minute;
    int m_second;
/// @endcond
};