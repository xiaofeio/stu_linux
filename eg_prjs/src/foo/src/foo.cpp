﻿#include "foo.h"

int add(int a, int b) {
    return a + b;
}


FXXDateTime::FXXDateTime(int year, int month, int day, int hour, int minute, int second)
    : m_year(year)
    , m_month(month)
    , m_day(day)
    , m_hour(hour)
    , m_minute(minute)
    , m_second(second)
{}

FXXDateTime::~FXXDateTime()
{
}